import React from 'react';
import './IngredientsControl.css';

const IngredientsControl = (props) => {
  console.log(props);
  return(
    <div className="IngredientsControl">
      <p>Current Price: <span className="IngredientsControl-bold">{props.price} soms</span></p>
      {
        props.food.map((item, index) => {
          return (
            <div key={index}>
              <span className="IngredientsControl-bold">{item.type}</span>
              <div className="IngredientsControl-b">
                <button onClick={() => props.remove(index)} className="IngredientsControl-bnt remove">Less</button>
                <button onClick={() => props.click(index)} className="IngredientsControl-bnt click">More</button>
              </div>
            </div>
          )
        })
      }
    </div>
  )
};

export default IngredientsControl;