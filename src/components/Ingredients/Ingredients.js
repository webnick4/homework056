import React from 'react';
import './Ingredients.css';

const Ingredients = (props) => {

  return(
    <div className="Burger">
      <div className="BreadTop">
        <div className="Seeds1"></div>
        <div className="Seeds2"></div>
      </div>
      { props.food.map((item, index) => {
        return <div className={item} key={index}></div>;
      })}
      <div className="BreadBottom"></div>
    </div>
  )
};

export default Ingredients;