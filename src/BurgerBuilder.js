import React, { Component } from 'react';
import './BurgerBuilder.css';
import Ingredients from "./components/Ingredients/Ingredients";
import IngredientsControl from "./components/IngredientsControl/IngredientsControl";



class BurgerBuilder extends Component {

  state = {
    food: [
      {type: 'Salad', amount: 0, price: 5},
      {type: 'Cheese', amount: 0, price: 20},
      {type: 'Meat', amount: 0, price: 50},
      {type: 'Bacon', amount: 0, price: 30}
    ],
    totalPrice: 20,
    arrayIngredients: []
  };

  amountIncrease = (index) => {
    const food = [...this.state.food];
    const ingredients = {...food[index]};
    ingredients.amount++;
    food[index] = ingredients;

    let totalPrice = this.state.totalPrice;
    totalPrice += this.state.food[index].price;

    this.state.arrayIngredients.push(ingredients.type);

    this.setState({food, totalPrice});
  };

  removeIngredients = (index) => {
    const food = [...this.state.food];
    const ingredients = {...food[index]};

    if (ingredients.amount >= 1) {
      ingredients.amount--;
      food[index] = ingredients;
    }

    const type = ingredients.type;

    const array = [...this.state.arrayIngredients];
    const arr = [...array];
    const ingredientIndex =  arr.indexOf(type);

    if (ingredientIndex !== -1) {
      this.state.arrayIngredients.splice(ingredientIndex, 1);
    }

    let totalPrice = this.state.totalPrice;
    if (totalPrice > 20 && ingredientIndex !== -1) {
      totalPrice -= this.state.food[index].price;
    }

    this.setState({food, totalPrice});
  };

  render() {
    return (
      <div className="BurgerBuilder">
        <Ingredients food={this.state.arrayIngredients}/>
        <IngredientsControl click={this.amountIncrease} remove={this.removeIngredients} food={this.state.food} price={this.state.totalPrice}/>
      </div>
    );
  }
}

export default BurgerBuilder;
